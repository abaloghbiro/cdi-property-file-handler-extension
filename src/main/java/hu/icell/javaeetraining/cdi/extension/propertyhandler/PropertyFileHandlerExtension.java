package hu.icell.javaeetraining.cdi.extension.propertyhandler;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.InjectionException;
import javax.enterprise.inject.spi.AnnotatedField;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.enterprise.inject.spi.InjectionTarget;
import javax.enterprise.inject.spi.ProcessInjectionTarget;

public class PropertyFileHandlerExtension implements Extension {

	private final Map<Field, Object> fieldValues = new HashMap<>();

	public <T> void initializePropertyLoading(final @Observes ProcessInjectionTarget<T> pit) {

		AnnotatedType<T> at = pit.getAnnotatedType();
		if (!at.isAnnotationPresent(PropertyFile.class)) {
			return;
		}

		PropertyFile propertyFile = at.getAnnotation(PropertyFile.class);
		String propertyFileName = propertyFile.fileName();
		InputStream propertiesStream = getClass().getResourceAsStream("/" + propertyFileName);
		Properties properties = new Properties();
		try {
			properties.load(propertiesStream);
			assignPropertiesToFields(at.getFields(), properties); // Implementation follows
		} catch (IOException e) {
			e.printStackTrace();
		}

		final InjectionTarget<T> it = pit.getInjectionTarget();

		InjectionTarget<T> wrapped = new InjectionTarget<T>() {
			@Override
			public void inject(T instance, CreationalContext<T> ctx) {
				// ITT INJEKTALJUK AZ EREDTI DEPEKET!!
				it.inject(instance, ctx);

				for (Map.Entry<Field, Object> property : fieldValues.entrySet()) {
					try {
						Field fieldInManagedClazz = property.getKey();
						fieldInManagedClazz.setAccessible(true);
						Class<?> baseType = fieldInManagedClazz.getType();
						String value = property.getValue().toString();
						if (baseType == String.class) {
							fieldInManagedClazz.set(instance, value);
						} else if (baseType == Integer.class) {
							fieldInManagedClazz.set(instance, Integer.valueOf(value));
						} else if (baseType == Double.class) {
							fieldInManagedClazz.set(instance, Double.valueOf(value));
						} else {
							pit.addDefinitionError(new InjectionException("Type " + baseType + " of Field "
									+ fieldInManagedClazz.getName() + " not recognized yet!"));
						}
					} catch (Exception e) {
						pit.addDefinitionError(new InjectionException(e));
					}
				}
			}

			@Override
			public void postConstruct(T instance) {
				it.postConstruct(instance);
			}

			@Override
			public void preDestroy(T instance) {
				it.dispose(instance);
			}

			@Override
			public void dispose(T instance) {
				it.dispose(instance);
			}

			@Override
			public Set<InjectionPoint> getInjectionPoints() {
				return it.getInjectionPoints();
			}

			@Override
			public T produce(CreationalContext<T> ctx) {
				return it.produce(ctx);
			}
		};
		pit.setInjectionTarget(wrapped);
	}

	private <T> void assignPropertiesToFields(Set<AnnotatedField<? super T>> fields, Properties properties) {
		for (AnnotatedField<? super T> field : fields) {
			if (field.isAnnotationPresent(Property.class)) {
				Property property = field.getAnnotation(Property.class);
				String value = properties.getProperty(property.propertyName());
				fieldValues.put(field.getJavaMember(), value);
			}
		}
	}

}